package com.gemini_is.utils

import java.io.IOException
import java.util.*

class SystemUtils {
    companion object {
        fun loadResourcesFile() {
            val props = Properties()
            val resource = SystemUtils::class.java
                .classLoader
                .getResourceAsStream("gemini.properties")
            if (resource != null) {
                try {
                    props.load(resource)
                } catch (error: IOException) {
                    error.printStackTrace()
                }
                System.getProperties().forEach { key: Any?, value: Any? -> props[key] = value }
                System.setProperties(props)
            }
        }

        fun getVariable(variableName: String): String? {
            loadResourcesFile()
            return System.getenv(variableName) ?: System.getProperty(variableName) ?: null
        }
    }
}