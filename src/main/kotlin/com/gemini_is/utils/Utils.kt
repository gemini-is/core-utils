package com.gemini_is.utils

import java.util.*

class Utils {
    companion object {
        fun checkParams(vararg args: String?): Boolean {
            return if (args.isNotEmpty()) {
                Arrays.stream(args).allMatch { it.isNullOrBlank().not() }
            } else false
        }
    }
}
