# Changelog
## [3.1.0](https://gitlab.com/gemini-is/gemini-utils/tree/v3.1.0) - 2020-02-04
Added loadResourceFile() to SystemUtils
### Added
- SystemUtils:
  - Added loadResourceFile() function. This functions only loads properties
  inside gemini.properties to System.properties.
### Changed
- SystemUtils:
  - getVariable():
    - getVariable() now loads system properties by first calling to 
    loadResourceFile().
## [3.0.0](https://gitlab.com/gemini-is/gemini-utils/tree/v3.0.0) - 2020-01-01
Changed properties file to gemini.properties.
### Changed:
- SystemUtils (**BREAKING**):
  - Changed searched properties file from db.properties to gemini.properties.
## [2.0.1](https://gitlab.com/gemini-is/gemini-utils/tree/v2.0.1) - 2019-12-30
Fixed bug that avoided postgres driver to be registered.
### Added
- ConnectionManagerTest:
  - Added test "selectPostgresDriver" to assert postgres driver selection 
    when jdbc:postgres URI is provided.
### Changed
- pom.xml:
  - Changed postgres jdbc from 42.2.9 to 9.4-1206-jdbc4.
## [2.0.0](https://gitlab.com/gemini-is/gemini-utils/tree/v2.0.0) - 2019-12-30
Hotfix, tests & cleanup.
### Breaking:
- Moved ConnectionManager from com.gemini_is.models.sql to com.gemini_is.utils.sql.
### Added:
- ConnectionManagerTest test case with several assertions.
## [1.1.0](https://gitlab.com/gemini-is/gemini-utils/tree/v1.1.0) - 2019-12-29
Added first devops pipelines.    
### Added:
- Added .gitlab-ci.yml with customized auto-devops template.
- Added jar final name into jar plugin configuration.
### Deleted:
- Disabled default build job.
## [1.0.0](https://gitlab.com/gemini-is/gemini-utils/tree/v1.0.0) - 2019-12-29
First version.